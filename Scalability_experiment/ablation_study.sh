parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
folder_path=$parent_path"/ablation_study/"
results_path=$folder_path"results.csv"

alpha_size=5
outdegree=1.5
state_nb=10
k=2
split_ratio=0.25
tss_nb=500

rm -f ${results_path} # Delete old result file

# Splits
for id in {0..199}
do
{
    start=`gdate +%s%6N`
    res=`python "$parent_path"/tag_scalab_xp.py "${folder_path}traces-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}" $k "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}-${k}-split" True`
    end=`gdate +%s%6N`
} && {
    check=`gdate +%s%6N`
    runtime=$( echo "$end - $start" | bc -l )
    score=`python "$parent_path"/tag_scoring.py "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}-${k}-split" "${folder_path}traces-negatives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}" "${folder_path}traces-positives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}"`
    echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|$runtime|$res|$score|$k|splits" >> ${results_path}
}
if (( $check < $end ))
then
   echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|ERROR|ERROR|ERROR|$k|splits" >> ${results_path}
fi
done

# No splits
for id in {0..199}
do
{
    start=`gdate +%s%6N`
    res=`python "$parent_path"/tag_scalab_xp.py "${folder_path}traces-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}" $k "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}-${k}-no_split" False`
    end=`gdate +%s%6N`
} && {
    check=`gdate +%s%6N`
    runtime=$( echo "$end - $start" | bc -l )
    score=`python "$parent_path"/tag_scoring.py "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}-${k}-no_split" "${folder_path}traces-negatives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}" "${folder_path}traces-positives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}"`
    echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|$runtime|$res|$score|$k|no_splits" >> ${results_path}
}
if (( $check < $end ))
then
   echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|ERROR|ERROR|ERROR|$k|no_splits" >> ${results_path}
fi
done

# APTA only
# 
for id in {0..199}
do
{
    start=`gdate +%s%6N`
    res=`python "$parent_path"/tag_scalab_xp.py "${folder_path}traces-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}" $k "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}-${k}-split" False False`
    end=`gdate +%s%6N`
} && {
    check=`gdate +%s%6N`
    runtime=$( echo "$end - $start" | bc -l )
    score=`python "$parent_path"/tag_scoring.py "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}-${k}-split" "${folder_path}traces-negatives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}" "${folder_path}traces-positives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}"`
    echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|$runtime|$res|$score|$k|apta" >> ${results_path}
}
if (( $check < $end ))
then
   echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|ERROR|ERROR|ERROR|$k|apta" >> ${results_path}
fi
done
