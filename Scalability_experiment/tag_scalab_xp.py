"""
TAG launcher script
Author: 
"""

from datetime import datetime
import sys, os

sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/TAG/")

from TAG.TALearner import TALearner
t0 = datetime.now()
if len(sys.argv) == 4:
	l = TALearner(sys.argv[1], k=eval(sys.argv[2]), res_path=sys.argv[3])
elif len(sys.argv) == 5:
	l = TALearner(sys.argv[1], k=eval(sys.argv[2]), res_path=sys.argv[3], splits=sys.argv[4])
else:
    l = TALearner(sys.argv[1], k=eval(sys.argv[2]), res_path=sys.argv[3], splits=sys.argv[4], merges=eval(sys.argv[5]))
print(datetime.now() - t0)
