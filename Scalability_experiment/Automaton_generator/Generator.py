"""
Timed Automata generator
Author: 
"""

import random
import sys, os

sys.path.append(os.path.split(os.path.abspath(os.path.dirname(sys.argv[0])))[0]+'/TAG')

from TAG.Automaton import Automaton

class Generator:

    def __init__(self, state_nb, edge_nb, alpha_size, split_ratio, min_guard, max_guard):
        self.ta_nb = 0
        self.state_nb = state_nb
        self.edge_nb = edge_nb
        self.alpha_size = alpha_size
        self.split_ratio = split_ratio
        self.min_guard = min_guard
        self.max_guard = max_guard
        self.tas = list()
        self.checkParams()

    def checkParams(self):
        if self.state_nb > self.edge_nb+1:
            raise ValueError("Minimal number of edges expected: " + str(self.state_nb-1))
        if self.alpha_size > round(self.edge_nb*(1-self.split_ratio/2)):
            self.split_ratio = 2 * (1-self.alpha_size/self.edge_nb)
            if self.split_ratio < 0: raise ValueError("Minimal edge number expected: " + str(round(self.alpha_size / (1 - self.split_ratio / 2))))

    def generate(self, k=1):
        self.ta_nb += k
        n = len(self.tas)
        failed = 0
        while n < self.ta_nb or failed > 100:
            ta = self.make()
            if ta is None or not self.okParams(ta): failed += 1
            else:
                self.tas.append(ta)
                n += 1
                failed = 0

    def select_source(self, ta):
        candidates = list()
        for s in ta.states:
            if len(s.edges_out) < self.alpha_size and len(s.edges_in) > 0:
                candidates.append(s)
        return candidates

    def isolate_states(self, ta):
        res = list()
        for s in ta.states:
            if len(s.edges_in) == 0 and not s.initial: res.append(s)
        return res

    def action_choice(self, alpha, state):
        res = dict()
        for a in alpha:
            if a not in [e.symbol for e in state.edges_out]:
                res[a] = 1/(alpha[a]+1) if alpha[a]>0 else 200
        return res

    def splitCandidates(self, ta):
        res = list()
        for e in ta.edges:
            if len(e.destination.edges_in) < 2 and \
                    max(e.guard) - min(e.guard) > 1 and \
                    len(e.destination.edges_out) > 1:
                res.append(e)
        if len(res) == 0:
            for e in ta.edges:
                if max(e.guard) - min(e.guard) > 1 and \
                        len(e.destination.edges_out) > 1:
                    res.append(e)
        return res

    def targetCandidates(self, ta, source):
        res = list()
        for s in ta.states:
            if s not in [e.destination for e in source.edges_out]:
                res.append(s)
        return res

    def make(self):
        ta = Automaton()
        alpha = dict.fromkeys([chr(i+ord('a')) for i in range(0, self.alpha_size)], 0)
        for i in range(1, self.state_nb):
            ta.add_state('S'+str(i))
        source = ta.search_state('S0')
        action = random.choice(list(alpha.keys()))
        alpha[action] += 1
        target = random.choice([s for s in ta.states if s != source])
        ta.add_edge(source.name, target.name, action, [self.min_guard, self.max_guard])
        if action not in ta.symbols: ta.symbols.append(action)
        source = target
        while len(ta.edges) < round(self.edge_nb*(1-self.split_ratio/2)):
            if len(self.isolate_states(ta)) > 0:
                target = random.choice(self.isolate_states(ta))
            else:
                candidates = self.targetCandidates(ta, source)
                if len(candidates) == 0: target = source
                else: target = random.choice(candidates)
            actions = self.action_choice(alpha, source)
            action = random.choices(list(actions.keys()), list(actions.values()))[0]
            alpha[action] += 1
            interval = random.sample(range(self.min_guard, self.max_guard), k=2)
            ta.add_edge(source.name, target.name, action, [min(interval), max(interval)])
            if action not in ta.symbols: ta.symbols.append(action)
            source = random.choice(self.select_source(ta))
        for i in range(len(ta.edges), self.edge_nb):
            candidates = self.splitCandidates(ta)
            if len(candidates) == 0: return None
            edge = random.choice(candidates)
            tv = random.randint(min(edge.guard)+1, max(edge.guard)-1)
            if len(self.isolate_states(ta)) > 0:
                target = random.choice(self.isolate_states(ta))
            else:
                liste = [s for s in self.targetCandidates(ta, edge.source) if s != edge.source]
                if not liste: return
                target = random.choice(liste)
            ta.add_edge(edge.source.name, target.name, edge.symbol, [min(edge.guard), tv])
            if action not in ta.symbols: ta.symbols.append(action)
            edge.guard.remove(min(edge.guard))
            edge.guard.append(tv+1)
        if len(self.isolate_states(ta)) > 0: return None
        return ta

    def okParams(self, ta):
        if len(ta.states) != self.state_nb: return False
        if len(ta.edges) != self.edge_nb: return False
        if len(ta.symbols) != self.alpha_size: return False
        return True;

