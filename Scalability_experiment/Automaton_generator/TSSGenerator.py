
"""
Timed string generator
Author: 
"""

import sys, os
sys.path.append(os.path.split(os.path.abspath(os.path.dirname(sys.argv[0])))[0]+'/TAG')

from TAG.Automaton import Automaton
from TAG.State import State
import random

class TSSGenerator:
    def __init__(self, dot_path, n_tss=100, final_states=['S0'], p_fin={'S0': 0.2}, initial_state='S0', unique=False):
        """
        Timed string generator
        Args:
            dot_path (str): Path of the DOT file
            n_tss (int): Number of timed string to generate
            final_states (list): List of final states
            p_fin (dict/float): Dictionary of the ending probability (value) for each final state (key) or float value for all the final states
            initial_state (str): Name of the initial state
        Returns:
            None
        """
        self.ta = Automaton(dot_path)
        if initial_state != 'S0':
            self.ta.search_state(initial_state).initial = True
        self.dot_path = dot_path
        self.final_states = final_states
        if isinstance(p_fin, float) or isinstance(p_fin, int):
            if final_states == ['S0']: self.final_states = [s.name for s in self.ta.states]
            self.p_fin = dict.fromkeys(self.final_states, p_fin)
        elif isinstance(p_fin, dict):
            self.p_fin = p_fin
        for state in self.ta.states:
            if len(state.edges_out) == 0 or \
                    (len(set([e.destination for e in state.edges_out])) == 1 and
                     [e.destination for e in state.edges_out][0] == state):
                self.final_states.append(state.name)
                self.p_fin[state.name] = 1
        self.n_tss = n_tss
        self.__tss = list()
        self.unique = unique

    def get_initial_state(self) -> State:
        """
        Return the initial state object
        Returns:
            State: Initial state object of the TA
        """
        for state in self.ta.states:
            if state.initial:
                return state

    def generate_negative_ts(self, time_error, edge_error):
        ts = list()
        state = self.get_initial_state()
        error = False
        max_tv = max([max(e.guard) for e in self.ta.edges])
        min_tv = min([min(e.guard) for e in self.ta.edges])
        while True:
            edges = list(state.edges_out)
            p_error = random.random()
            if p_error > 0.8 and time_error:
                edge = random.choices(edges)[0]
                tvs = [tv for e in state.search_edges(edge.symbol, 'out') for tv in e.guard]
                if max_tv - max(tvs) + min(tvs) - min_tv > 0:
                    tv = random.choice(list(range(min_tv, min(tvs)))+list(range(max(tvs)+1, max_tv+1)))
                    error = True
                else:
                    tv = random.randint(min(edge.guard), max(edge.guard))
            elif p_error > 0.8 and edge_error:
                edge = random.choices([e for e in self.ta.edges if e not in edges])[0]
                tv = random.randint(min(edge.guard), max(edge.guard))
                tvs = [tv for e in state.search_edges(edge.symbol, 'out') for tv in e.guard]
                if len(tvs) == 0 or not (min(tvs) <= tv <= max(tvs)):
                    error = True
                else:
                    edge = self.ta.next_edge(state.name, edge.symbol, tv)
            else:
                edge = random.sample(state.edges_out, 1)[0]
                tv = random.randint(min(edge.guard), max(edge.guard))
            ts.append((edge.symbol, tv))
            state = edge.destination
            if error:
                return ts
            if state.name in self.final_states:
                if self.p_fin[state.name] < 1.0 and not error: continue
                elif self.p_fin[state.name] == 1.0 and not error:
                    ts = list()
                    state = self.get_initial_state()
                    continue
                else:
                    p = random.random()
                    if p <= self.p_fin[state.name]:
                        return ts

    def generate_ts(self):
        """
        Generate a timed string consistent with the TA
        Returns:
            list: List of tuples (symbol, time value) starting from the initial state and ending in a final state
        """
        ts = list()
        state = self.get_initial_state()
        while True:
            edge = random.sample(state.edges_out, 1)[0]
            tv = random.randint(min(edge.guard), max(edge.guard))
            ts.append((edge.symbol, tv))
            state = edge.destination
            if state.name in self.final_states:
                p = random.random()
                if p <= self.p_fin[state.name]:
                    return ts

    def get_tss(self):
        """
        Getter for the timed string list
        Returns:
            list: List of the generated timed strings
        """
        return self.__tss

    def generate(self, positive=True):
        """
        Generate the correct amount of timed strings
        Returns:
            None
        """
        if positive:
            for i in range(0, self.n_tss):
                self.__tss.append(self.generate_ts())
        else:
            for i in range(0, int(self.n_tss/2)):
                again = True
                if self.unique:
                    while(again):
                        ts = self.generate_negative_ts(True, False)
                        if ts not in self.__tss:
                            again = False
                            self.__tss.append(ts)
                else:
                    self.__tss.append(self.generate_negative_ts(True, False))
            for i in range(int(self.n_tss / 2), self.n_tss):
                again = True
                if self.unique:
                    while(again):
                        ts = self.generate_negative_ts(False, True)
                        if ts not in self.__tss:
                            again = False
                            self.__tss.append(ts)
                else:
                    self.__tss.append(self.generate_negative_ts(False, True))

    def tag_file_writer(self, file):
        """
        Create a file with the generated timed string with the syntax of TAG
        Args:
            file (str): The file to create
        Returns:
            None
        """
        f = open(file, 'w+')
        for ts in self.__tss[:-1]:
            for t in ts[:-1]:
                f.write(t[0] + ':' + str(t[1]) + ' ')
            f.write(ts[-1][0] + ':' + str(ts[-1][1]))
            f.write('\n')
        for t in self.__tss[-1][:-1]:
            f.write(t[0] + ':' + str(t[1]) + ' ')
        f.write(self.__tss[-1][-1][0] + ':' + str(self.__tss[-1][-1][1]))
        f.close()

    def rtip_file_writer(self, file):
        """
        Create a file with the generated timed string with the syntax of RTI+
        Args:
            file (str): The file to create
        Returns:
            None
        """
        f = open(file, 'w+')
        f.write(str(len(self.__tss)) + ' ' + str(len(self.ta.symbols)) + '\n')
        for ts in self.__tss[:-1]:
            f.write(str(len(ts)) + ' ')
            for t in ts[:-1]:
                f.write(t[0] + ' ' + str(t[1]) + ' ')
            f.write(ts[-1][0] + ' ' + str(ts[-1][1]))
            f.write('\n')
        f.write(str(len(self.__tss[-1])) + ' ')
        for t in self.__tss[-1][:-1]:
            f.write(t[0] + ' ' + str(t[1]) + ' ')
        f.write(self.__tss[-1][-1][0] + ' ' + str(self.__tss[-1][-1][1]))
        f.close()

    def tkt_file_writer(self, folder):
        """
        Create a file with the generated timed string with the syntax of Timed k-Tail
        Args:
            file (str): The folder where to create the timed string files
        Returns:
            None
        """
        if not os.path.exists(folder):
            os.mkdir(folder)
        cpt = 0
        for ts in self.__tss:
            gtime = 0
            f = open(folder + '/ts_' + str(cpt) + '.csv', 'w+')
            f.write('START' + '\n')
            for t in ts:
                f.write('process;' + t[0] + ';' + 'B' + ';' + str(gtime) + '\n')
                gtime += t[1]
                f.write('process;' + t[0] + ';' + 'E' + ';' + str(gtime) + '\n')
            f.write('STOP' + '\n')
            f.close()
            cpt += 1

    def empty_tss(self):
        self.__tss = list()
