"""
Scoring script for TAG automata
Author: 
"""

import sys, os
import re

sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/TAG/")

from TAG.Automaton import Automaton

ta = Automaton(sys.argv[1])

tss_file = open(sys.argv[2])
tss = tss_file.readlines()
tss_file.close()
neg = []
for ts in tss:
    ts = re.sub('\\n', '', ts)
    ts = ts.split(' ')
    neg.append(ts)

res = ta.inconsistency_nb(neg, True, False, False)
print(res, end="|")

tss_file = open(sys.argv[3])
tss = tss_file.readlines()
tss_file.close()
pos = []
for ts in tss:
    ts = re.sub('\\n', '', ts)
    ts = ts.split(' ')
    pos.append(ts)

res = ta.inconsistency_nb(pos, True, False, False)
print(res)
