#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(shinythemes)

library(stringr)
library(ggplot2)
library(tidyverse)
library(ggpubr)
library(car)
library(rstatix)
library(patchwork)

# DATA IMPORTATION

data <- c('./1-alpha_size/results.csv', './2-outdegree/results.csv',  './3-state_nb/results.csv', './4-split_ratio/results.csv', './5-tss_nb/results.csv', './6-k/results.csv')

format_time <- function(x){
    m = str_match(x, "(\\d+):(\\d+):(\\d+).(\\d+)")
    if (!is.na(m[,1])){
        return((eval(parse(text=m[,2]))*60*60*1000000 + eval(parse(text=m[,3]))*60*1000000 + eval(parse(text=m[,4]))*1000000 + eval(parse(text=m[,5])))/10^6)
    } else {
        return(eval(parse(text=x))/10^6)
    }
}

import_data <- function(file){
    colnames <- c('timestamp', 'algo', 'id', 'alpha_size', 'outdegree', 
                  'state_nb', 'split_ratio', 'tss_nb', 'runtime', 'tag_runtime', 
                  'TN', 'FN', 'k')
    coltypes <- c('timestamp'='character', 'algo'='character', 'id'='numeric', 
                  'alpha_size'='numeric', 'outdegree'='numeric', 'state_nb'='numeric', 
                  'split_ratio'='numeric', 'tss_nb'='numeric', 'runtime'='numeric', 
                  'tag_runtime'='character', 'TN'='numeric', 'FN'='numeric', 'k'='numeric')
    dataset <- read.csv(file, header = FALSE, sep = '|', col.names = colnames, colClasses = coltypes)
    dataset["tag_runtime"] = apply(dataset["tag_runtime"], MARGIN=1, FUN=format_time)
    dataset["runtime"] = apply(dataset["runtime"], MARGIN=1, FUN=function(x) x/10^6)
    dataset["TNR"] = apply(dataset["TN"], MARGIN=1, FUN=function(x) x/100)
    dataset["TPR"] = apply(dataset["FN"], MARGIN=1, FUN=function(x) 1-(x/100))
    dataset["FP"] = apply(dataset["TN"], MARGIN=1, FUN=function(x) 100-x)
    dataset["TP"] = apply(dataset["FN"], MARGIN=1, FUN=function(x) 100-x)
    dataset["F1"] = apply(dataset, MARGIN=1, FUN=function(x) 2*as.numeric(x['TP'])/(2*as.numeric(x["TP"])+as.numeric(x["FP"])+as.numeric(x["FN"])))
    dataset["ACC"] = apply(dataset, MARGIN=1, FUN=function(x) (as.numeric(x['TP'])+as.numeric(x['TN']))/200)
    dataset["MCC"] = apply(dataset, MARGIN=1, FUN=function(x) (as.numeric(x['TP'])*as.numeric(x['FN'])-as.numeric(x['FP'])*as.numeric(x['FN']))/(sqrt((as.numeric(x['TP'])+as.numeric(x['FP']))*(as.numeric(x['TP'])+as.numeric(x['FN']))*(as.numeric(x['TN'])+as.numeric(x['FP']))*(as.numeric(x['TN'])+as.numeric(x['FN'])))))
    dataset["PPV"] = apply(dataset, MARGIN=1, FUN=function(x) (as.numeric(x['TP'])/(as.numeric(x['TP']) + as.numeric(x['FP']))))
    return(dataset)
}

data <- lapply(data, import_data)

res <- do.call("rbind", data)

ui <- fluidPage(
    theme = shinytheme("flatly"),
    titlePanel("TAG: Scalability experiment"),
    tabsetPanel(
        tabPanel("Global results", fluid = TRUE,
            mainPanel(
                plotOutput("global_runtime"),
                plotOutput("global_scores")
            )
        ),
        
        tabPanel("Result per factor", fluid = TRUE,
            sidebarLayout(
                sidebarPanel(
                    selectInput("variable",
                                "Factor:",
                                choices=c("1- Alphabet size" = "alpha_size", "2- Outdegree" = "outdegree", "3- State number" = "state_nb",
                                          "4- Twinned transition proportion" = "split_ratio", "5- Timed string number" = "tss_nb",
                                          "6- k" = "k")),
                    radioButtons("quanti_quali",
                                 "Variable(s):",
                                 choices=c("Runtime", "Scores")),
                    radioButtons("graph_type",
                                 "Output type:",
                                 choices=c("Lineplot"="l", "Boxplot"="b", "Statistical analysis"="a")),
                    radioButtons("stars",
                                 "Show pairwise-test significal difference asterisk:",
                                 choices=c("Yes"=TRUE, "No"=FALSE))
                ),
                mainPanel(
                    tabsetPanel(id = "tabs",
                        tabPanel(
                            "All", value = "All", conditionalPanel("input.graph_type != 'a'", plotOutput("all_plot")), 
                            conditionalPanel("input.graph_type == 'a'", verbatimTextOutput("resAnovaAll"))
                        ),
                        tabPanel(
                            "TAG", value = "TAG", conditionalPanel("input.graph_type != 'a'", plotOutput("tag_plot")), 
                            conditionalPanel("input.graph_type == 'a'", verbatimTextOutput("resAnovaTAG"))
                        ),
                        tabPanel(
                            "RTI+", value = "RTI+", conditionalPanel("input.graph_type != 'a'", plotOutput("rtip_plot")), 
                            conditionalPanel("input.graph_type == 'a'", verbatimTextOutput("resAnovaRTIP"))
                        ),
                        tabPanel(
                            "Timed k-Tail", value = "TkT", conditionalPanel("input.graph_type != 'a'", plotOutput("tkt_plot")), 
                            conditionalPanel("input.graph_type == 'a'", verbatimTextOutput("resAnovaTKT"))
                        )
                    )
                )
            )
        )
    )
)

server <- function(input, output) {
    
    output$global_runtime <- renderPlot({
        g0 <- ggboxplot(res, x = "algo", y = "runtime", ylab = "Runtime (s) (in log scale)", 
                        xlab = "Algorithm", title = "Runtime per algorithm", subtitle = "On whole results.") + 
            scale_x_discrete(labels=c("TAG"="TAG","TkT"="Timed k-Tail","RTI+"="RTI+"))
        pwc <- pairwise_t_test(res, runtime ~ algo, p.adjust.method = "bonferroni") 
        pwc$p_simpl = lapply(pwc$p.adj, FUN = function(x) if (x<0.05) "*" else "ns")
        pwc <- pwc %>% add_xy_position(y.trans=log2)
        g1 <- g0 + stat_pvalue_manual(pwc, label="p_simpl", hide.ns = TRUE, step.increase = 0.1, size = 8) + 
            scale_y_continuous(trans = "log2") + theme_pubr(base_size = 18) 
        print(g1)
    })
    
    output$global_scores <- renderPlot({
        g0 <- ggboxplot(res, x = "algo", y = cbind('PPV', 'TPR', 'F1'), combine = TRUE, ylab = "Score", 
                        xlab = "Algorithm", panel.labs = list(.y. = c('Precision', 'Recall', 'F1-score')), 
                        title = "Scores per algorithm", subtitle = "On whole results.") + 
            scale_x_discrete(labels=c("TAG"="TAG","TkT"="Timed\nk-Tail","RTI+"="RTI+"))
        pwc_f1 <- pairwise_t_test(res, F1 ~ algo, p.adjust.method = "bonferroni")
        pwc_f1 <- pwc_f1 %>% add_xy_position(x = "algo")
        pwc_f1$p_simpl = lapply(pwc_f1$p.adj, FUN = function(x) if (x<0.05) "*" else "ns")
        pwc_ppv <- pairwise_t_test(res, PPV ~ algo, p.adjust.method = "bonferroni")
        pwc_ppv <- pwc_ppv %>% add_xy_position(x = "algo")
        pwc_ppv$p_simpl = lapply(pwc_ppv$p.adj, FUN = function(x) if (x<0.05) "*" else "ns")
        pwc_tpr <- pairwise_t_test(res, TPR ~ algo, p.adjust.method = "bonferroni")
        pwc_tpr <- pwc_tpr %>% add_xy_position(x = "algo")
        pwc_tpr$p_simpl = lapply(pwc_tpr$p.adj, FUN = function(x) if (x<0.05) "*" else "ns")
        g0 + stat_pvalue_manual(pwc_f1, hide.ns = TRUE, y.position = 1.05, step.increase = 0.1, size = 8, label='p_simpl') + stat_pvalue_manual(pwc_ppv, hide.ns = TRUE, y.position = 1.05, step.increase = 0.1, size = 8, label='p_simpl') + stat_pvalue_manual(pwc_tpr, hide.ns = TRUE, y.position = 1.05, step.increase = 0.1, size = 8, label='p_simpl') + 
            scale_y_continuous(expand = expansion(mult = c(0.05, 0.1)), breaks=c(0.25, 0.5, 0.75, 1)) + theme(text=element_text(size = 15), axis.text.x = element_text(angle = 45, hjust=1))
    })

    get_dataset <- function(variable){
        if (variable == "alpha_size") return(data[[1]])
        if (variable == "outdegree") return(data[[2]])
        if (variable == "state_nb") return(data[[3]])
        if (variable == "split_ratio") return(data[[4]])
        if (variable == "tss_nb") return(data[[5]])
        if (variable == "k") return(data[[6]])
    }
    
    get_fname <- function(variable){
        if (variable == "alpha_size") return("Alphabet size")
        if (variable == "outdegree") return("Outdegree")
        if (variable == "state_nb") return("State number")
        if (variable == "split_ratio") return("Twinned transitions proportion")
        if (variable == "tss_nb") return("Timed string number")
        if (variable == "k") return("k")
    }
    
    p_r_l_a <- function(variable){
        df <- get_dataset(variable)
        g <- ggline(
            df, x = variable, y = "runtime", add = "mean", color = "algo",
            numeric.x.axis = TRUE, ylab = "Runtime (s) (in log scale)", xlab = get_fname(variable),
            shape = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"), size = 1) + scale_y_continuous(trans = 'log2') + 
            labs(color = "Algorithm", shape = "Algorithm") + theme(text=element_text(size=18))
        return(g)
    }
    
    p_s_l_a <- function(variable){
        df <- get_dataset(variable)
        size = 27
        linewidth = 1.5
        p0 <- ggline(df, x = "TPR", y = "PPV", add = "mean", color = "algo",
                    numeric.x.axis = TRUE, ylab = "Precision", xlab = "Recall",
                    shape = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"), size = linewidth) + 
            labs(color = "Algorithm", shape = "Algorithm") + xlim(0, 1) + ylim(0, 1) +
            theme(text = element_text(size = size)) + geom_abline(intercept = 1, slope = -1, linetype="dashed", color = "darkgray")
        p1 <- ggline(df, x = variable, y = "PPV", add = "mean", color = "algo",
                    numeric.x.axis = TRUE, ylab = "Precision", xlab = get_fname(variable),
                    shape = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"), size = linewidth) + 
            labs(color = "Algorithm", shape = "Algorithm") +
            theme(text = element_text(size = size)) 
        p2 <- ggline(df, x = variable, y = "TPR", add = "mean", color = "algo",
                    numeric.x.axis = TRUE, ylab = "Recall", xlab = get_fname(variable),
                    shape = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"), size = linewidth) + 
            labs(color = "Algorithm", shape = "Algorithm") + 
            theme(text = element_text(size = size)) 
        return(p0+p1+p2+plot_layout(ncol=1))
    }
    
    p_s_b_a <- function(variable){
        df <- get_dataset(variable)
        p0 <- ggboxplot(
            df, x = variable, y = "PPV", color = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"), 
            outlier.shape = NA, 
            xlab = get_fname(variable))
        p1 <- ggboxplot(
            df, x = variable, y = "TPR", color = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"), 
            outlier.shape = NA, 
            xlab = get_fname(variable))
        p2 <- ggboxplot(
            df, x = variable, y = "F1", color = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"), 
            outlier.shape = NA, 
            xlab = get_fname(variable))
        return(p0+p1+p2+plot_layout(ncol=1))
    }
    
    p_r_b_a <- function(variable){
        df <- get_dataset(variable)
        ylim1 = boxplot.stats(df$runtime)$stats[c(1, 5)]
        g <- ggboxplot(
            df, x = variable, y = "runtime",
            color = "algo", palette = c("#08306B", "#238B45", "#FD8D3C"),
            outlier.shape = NA, xlab = get_fname(variable), ylab = "Runtime (s) (in log scale)"
        ) + scale_y_continuous(trans = 'log2', limits = c(NA, ylim1[2])) + 
            labs(color = "Algorithm", shape = "Algorithm") 
        return(g)
    }
    
    # Plot score boxplot
    p_s_b_o <- function(learner, variable){
        df <- get_dataset(variable)
        if (input$stars){
            # TPR
            score <- "TPR"
            fm <- as.formula(paste(score, "~", variable))
            pwc <- pairwise_t_test(df[df["algo"] == learner, ], fm, p.adjust.method = "bonferroni")
            pwc <- pwc %>% add_xy_position(x = variable)
            p0 <- ggboxplot(df[df["algo"] == learner, ], x = variable, y = score, add = "point",
                          xlab = get_fname(variable), title = learner) +
                stat_pvalue_manual(pwc, hide.ns = TRUE, y.position = max(df[df["algo"] == learner, score])*1.05, step.increase = 0.1, size = 8) +
                theme(text = element_text(size = 17))
            # PPV
            score <- "PPV"
            fm <- as.formula(paste(score, "~", variable))
            pwc <- pairwise_t_test(df[df["algo"] == learner, ], fm, p.adjust.method = "bonferroni")
            pwc <- pwc %>% add_xy_position(x = variable)
            p1 <- ggboxplot(df[df["algo"] == learner, ], x = variable, y = score, add = "point",
                            xlab = get_fname(variable), title = learner) +
                stat_pvalue_manual(pwc, hide.ns = TRUE, y.position = max(df[df["algo"] == learner, score])*1.05, step.increase = 0.1, size = 8) +
                theme(text = element_text(size = 17))
            # F1
            score <- "F1"
            fm <- as.formula(paste(score, "~", variable))
            pwc <- pairwise_t_test(df[df["algo"] == learner, ], fm, p.adjust.method = "bonferroni")
            pwc <- pwc %>% add_xy_position(x = variable)
            p2 <- ggboxplot(df[df["algo"] == learner, ], x = variable, y = score, add = "point",
                            xlab = get_fname(variable), title = learner) +
                stat_pvalue_manual(pwc, hide.ns = TRUE, y.position = max(df[df["algo"] == learner, score])*1.05, step.increase = 0.1, size = 8) +
                theme(text = element_text(size = 17))
            return(p0+p1+p2+plot_layout(ncol=1))
        } else {
            # TPR
            score <- "TPR"
            p0 = (ggboxplot(df[df["algo"] == learner, ], x = variable, y = score, add = "point",
                            xlab = variable, title = learner, outlier.shape = NA))
            ylim1 = boxplot.stats(subset(df, algo==learner)[, score])$stats[c(1, 5)]
            p0 = p0 + ylim(NA, ylim1[2]*1.05) + ylab(score)
            # PPV
            score <- "PPV"
            p1 = (ggboxplot(df[df["algo"] == learner, ], x = variable, y = score, add = "point",
                            xlab = variable, title = learner, outlier.shape = NA))
            ylim1 = boxplot.stats(subset(df, algo==learner)[, score])$stats[c(1, 5)]
            p1 = p1 + ylim(NA, ylim1[2]*1.05) + ylab(score)
            # F1
            score <- "F1"
            p2 = (ggboxplot(df[df["algo"] == learner, ], x = variable, y = score, add = "point",
                            xlab = variable, title = learner, outlier.shape = NA))
            ylim1 = boxplot.stats(subset(df, algo==learner)[, score])$stats[c(1, 5)]
            p2 = p2 + ylim(NA, ylim1[2]*1.05) + ylab(score)
            return(p0+p1+p2+plot_layout(ncol=1))
        }
    }
    
    get_color <- function(learner){
        if (learner == "TAG") return("#238B45")
        if (learner == "RTI+") return("#08306B")
        if (learner == "TkT") return("#FD8D3C")
    }
    
    # Plot score line
    p_s_l_o <- function(learner, variable){
        df <- get_dataset(variable)
        size = 27
        linewidth = 1.5
        p0 <- ggline(df[df["algo"] == learner, ], x = "TPR", y = "PPV", add = "mean",
                     numeric.x.axis = TRUE, ylab = "Precision", xlab = "Recall",
                     color = get_color(learner), size = linewidth) + xlim(0, 1) + ylim(0, 1) +
            theme(text = element_text(size = size)) + geom_abline(intercept = 1, slope = -1, linetype="dashed", color = "darkgray")
        p1 <- ggline(df[df["algo"] == learner, ], x = variable, y = "PPV", add = "mean",
                     numeric.x.axis = TRUE, ylab = "Precision", xlab = get_fname(variable),
                     color = get_color(learner), size = linewidth) + 
            theme(text = element_text(size = size))
        p2 <- ggline(df[df["algo"] == learner, ], x = variable, y = "TPR", add = "mean",
                     numeric.x.axis = TRUE, ylab = "Recall", xlab = get_fname(variable),
                     color = get_color(learner), size = linewidth) + 
            theme(text = element_text(size = size))
        return(p0+p1+p2+plot_layout(ncol=1))
    }
    
    # Plot runtime line
    p_r_l_o <- function(learner, variable){
        df <- get_dataset(variable)
        size = 27
        linewidth = 1.5
        p0 <- ggline(df[df["algo"] == learner, ], x = variable, y = "runtime", add = "mean",
                     numeric.x.axis = TRUE, ylab = "Runtime", xlab = get_fname(variable),
                     color = get_color(learner), size = linewidth) + 
            theme(text = element_text(size = size))
        return(p0+plot_layout(ncol=1))
    }
    
    # Plot runtime boxplot
    p_r_b_o <- function(learner, variable){
        df <- get_dataset(variable)
        print(learner)
        if (input$stars){
            fm <- as.formula(paste("runtime ~", variable))
            pwc <- pairwise_t_test(df[df["algo"] == learner, ], fm, p.adjust.method = "bonferroni")
            pwc <- pwc %>% add_xy_position(x = variable)
            p0 <- ggboxplot(df[df["algo"] == learner, ], x = variable, y = "runtime", add = "point",
                            xlab = get_fname(variable), title = learner) +
                stat_pvalue_manual(pwc, hide.ns = TRUE, y.position = max(df[df["algo"] == learner, "runtime"])*1.05, step.increase = 0.1, size = 8) +
                theme(text = element_text(size = 17))
            return(p0+plot_layout(ncol=1))
        } else {
            p0 = (ggboxplot(df[df["algo"] == learner, ], x = variable, y = "runtime", add = "point",
                            xlab = variable, title = learner, outlier.shape = NA))
            ylim1 = boxplot.stats(subset(df, algo==learner)[, "runtime"])$stats[c(1, 5)]
            p0 = p0 + ylim(NA, ylim1[2]*1.05) + ylab("Runtime")
            return(p0+plot_layout(ncol=1))
        }
    }
    
    p_r_a_o <- function(learner, variable){
        df <- get_dataset(variable)
        fm <- as.formula(paste("runtime ~", variable))
        return(capture.output(Anova(lm(fm, data=df[df["algo"] == learner, ]), type="III", singular.ok = TRUE)))
    }
    p_s_a_o <- function(learner, variable){
        df <- get_dataset(variable)
        res <- c()
        for (score in c("PPV", "TPR", "F1")){
            fm <- as.formula(paste(score, "~", variable))
            #a <- capture.output(cat(capture.output(Anova(lm(fm, data=df[df["algo"] == learner, ]), type="III", singular.ok = TRUE)), sep='\n'))
            a <- capture.output(Anova(lm(fm, data=df[df["algo"] == learner, ]), type="III", singular.ok = TRUE))
            res <- c(res, a)
        }
        return(res)
    }
    p_r_a_a <- function(variable){
        df <- get_dataset(variable)
        fm <- as.formula(paste("runtime ~", variable))
        res <- c()
        for (learner in c("TkT", "TAG", "RTI+")){
            res <- c(res, c(learner))
            res <- c(res, capture.output(cat(p_r_a_o(learner, variable), sep='\n')))
        }
        return(res)
    }
    p_s_a_a <- function(variable){
        df <- get_dataset(variable)
        res <- c()
        for (learner in c("TkT", "TAG", "RTI+")){
            res <- c(res, c(learner))
            for (score in c("PPV", "TPR", "F1")){
                res <- c(res, capture.output(cat(p_s_a_o(learner, variable), sep='\n')))
            }
        }
        return(res)
    }

    plotall <- reactive(
        if (input$quanti_quali == "Runtime"){
            if (input$graph_type == "l"){
                return(p_r_l_a(input$variable))
            } else if (input$graph_type == "b"){
                return(p_r_b_a(input$variable))
            } else if (input$graph_type == "a"){
                return(p_r_a_a(input$variable))
            }
        } else {
            if (input$graph_type == "l"){
                return(p_s_l_a(input$variable))
            } else if (input$graph_type == "b"){
                return(p_s_b_a(input$variable))
            } else if (input$graph_type == "a"){
                return(p_s_a_a(input$variable))
            }
        }
    )
    
    plotone <- reactive(
        if (input$quanti_quali == "Runtime"){
            if (input$graph_type == "l"){
                return(p_r_l_o(input$tabs, input$variable))
            } else if (input$graph_type == "b"){
                return(p_r_b_o(input$tabs, input$variable))
            } else if (input$graph_type == "a"){
                return(p_r_a_o(input$tabs, input$variable))
            }
        } else {
            if (input$graph_type == "l"){
                return(p_s_l_o(input$tabs, input$variable))
            } else if (input$graph_type == "b"){
                return(p_s_b_o(input$tabs, input$variable))
            } else if (input$graph_type == "a"){
                return(p_s_a_o(input$tabs, input$variable))
            }
        }
    )
    
    get_h <- reactive({
        if (input$quanti_quali == "Scores") {
            return(1200)
        } else {
            return(400)
        }
    })
    
    observe({
        if (input$graph_type == "a"){
            output$resAnovaAll <- renderText(plotall(), quoted=FALSE, sep="\n")
            output$resAnovaTAG <- renderText(plotone(), quoted=FALSE, sep="\n")
            output$resAnovaTKT <- renderText(plotone(), quoted=FALSE, sep="\n")
            output$resAnovaRTIP <- renderText(plotone(), quoted=FALSE, sep="\n")
        }
    })
    
    observe({
        output$all_plot <- renderPlot({
            plotall()
        }, height=get_h())
    })
    
    observe({
        output$tkt_plot <- renderPlot({
            plotone()
        }, height=get_h())
    })
    observe({
        output$rtip_plot <- renderPlot({
            plotone()
        }, height=get_h())
    })
    observe({
        output$tag_plot <- renderPlot({
            plotone()
        }, height=get_h())
    })
}

shinyApp(ui = ui, server = server)
