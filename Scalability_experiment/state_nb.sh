parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
folder_path=$parent_path"/3-state_nb/"
results_path=$folder_path"results.csv"

alpha_size=5
outdegree=1.5
state_nb=10
k=2
split_ratio=0.25
tss_nb=500

rm -f ${results_path}

for state_nb in {4,6,8,10,15,25,50,75,100,500}
do
    # TAG
    for id in {162..199}
    do
	{
	    start=`gdate +%s%6N`
	    res=`python "$parent_path"/tag_scalab_xp.py "${folder_path}traces-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}" $k "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}"`
	    end=`gdate +%s%6N`
	} && {
	    check=`gdate +%s%6N`
	    runtime=$( echo "$end - $start" | bc -l )
	    score=`python "$parent_path"/tag_scoring.py "${folder_path}res-tag-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}" "${folder_path}traces-negatives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}" "${folder_path}traces-positives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}"`
	    echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|$runtime|$res|$score|$k" >> ${results_path}
	}
	if (( $check < $end ))
	then
	   echo "$(date +"%y-%m-%d %T")|TAG|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|ERROR|ERROR|ERROR|$k" >> ${results_path}
	fi
    done
    
    # Timed k-Tail
    for id in {0..199}
    do
    	{
    	    cd $parent_path
    	    start=`gdate +%s%6N`
    	    java -Dtkt.includeNestedCallsTime=false -cp tkt.jar it.unimib.disco.lta.timedKTail.ui.InferModel TA.jtml 1-alpha_size/traces-tkt-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}/ 1-alpha_size/res-tkt-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}
    	    end=`gdate +%s%6N`
    	} && {
    	    check=`gdate +%s%6N`
    	    runtime=$( echo "$end - $start" | bc -l )
    	    score=`python "$parent_path"/tkt_scoring.py "${folder_path}res-tkt-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}" "${folder_path}traces-negatives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}" "${folder_path}traces-positives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}"`
    	    echo "$(date +"%y-%m-%d %T")|TkT|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|$runtime|$runtime|$score|" >> ${results_path}
    	}
    	if (( $check < $end ))
    	then
    	   echo "$(date +"%y-%m-%d %T")|TkT|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|ERROR|ERROR|ERROR|" >> ${results_path}
    	fi
    done
    
    # RTI+
    for id in {0..199}
    do
    	{
    	    cd $parent_path/RTI/build
    	    start=`gdate +%s%6N`
    	    ./rti 1 0.05 ${folder_path}traces-rtip-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb} > ${folder_path}res-rtip-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}
    	    end=`gdate +%s%6N`
    	} && {
    	    check=`gdate +%s%6N`
    	    runtime=$( echo "$end - $start" | bc -l )
    	    score=`python "$parent_path"/rtip_scoring.py "${folder_path}res-rtip-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}-${tss_nb}" "${folder_path}traces-negatives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}" "${folder_path}traces-positives-${id}-${alpha_size}-${outdegree/./_}-${state_nb}-${split_ratio/./_}"`
    	    echo "$(date +"%y-%m-%d %T")|RTI+|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|$runtime|$runtime|$score|" >> ${results_path}
    	}
    	if (( $check < $end ))
    	then
    	   echo "$(date +"%y-%m-%d %T")|RTI+|${id}|${alpha_size}|${outdegree}|${state_nb}|${split_ratio}|${tss_nb}|ERROR|ERROR|ERROR|" >> ${results_path}
    	fi
    done
    echo "state number ${state_nb} done"
done

