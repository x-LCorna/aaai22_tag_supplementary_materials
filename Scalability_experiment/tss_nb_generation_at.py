import sys, os

sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/TAG/")
sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0]))+"/Automaton_generator/")

from TAG.Automaton import Automaton
from Automaton_generator.Generator import Generator
from Automaton_generator.TSSGenerator import TSSGenerator
import os
import shutil

outdegree = 1.5
state_nb = 10
min_guard, max_guard = 0, 20
tss_nb = 2500
alpha_size = 5
edge_nb = round(outdegree * state_nb)
split_ratio = 0.25

folder_path = os.path.abspath(os.path.dirname(sys.argv[0]))+'/5-tss_nb/'

g = Generator(state_nb, edge_nb, alpha_size, split_ratio, min_guard, max_guard)
g.generate(200)
id = 0
for ta in g.tas:
    path = folder_path + 'model_tas/' + 'ta-'+str(id)+'-'+str(alpha_size)+'-'+str(outdegree).replace('.', '_')+'-'+str(state_nb)+'-'+str(split_ratio).replace('.', '_')
    ta.export_ta(path)
    id += 1
print("tss_nb " + str(tss_nb) + ", " + str(len(g.tas)) + " TA generated.")

for id in range(0, 200):
    caracteristics = str(id)+'-'+str(alpha_size)+'-'+str(outdegree).replace('.', '_')+'-'+str(state_nb)+'-'+str(split_ratio).replace('.', '_')
    tss_nb = 2500
    g = TSSGenerator(folder_path+'model_tas/'+'ta-'+caracteristics, tss_nb, p_fin=0.15)
    g.generate()
    g.tag_file_writer(folder_path+'traces-tag-'+caracteristics+'-'+str(tss_nb))
    g.tkt_file_writer(folder_path+'traces-tkt-'+caracteristics+'-'+str(tss_nb))
    g.rtip_file_writer(folder_path+'traces-rtip-'+caracteristics+'-'+str(tss_nb))
    file = open(folder_path+'traces-tag-'+caracteristics+'-'+str(tss_nb), 'r')
    lines = file.readlines()
    file.close()
    for tss_nb in [50, 100, 500, 1000]:
        file = open(folder_path + 'traces-tag-' + caracteristics + '-' + str(tss_nb), 'w+')
        file.writelines(lines[:tss_nb])
        file.close()
    for tss_nb in [50, 100, 500, 1000]:
        if not os.path.exists(folder_path + 'traces-tkt-' + caracteristics + '-' + str(tss_nb)):
            os.mkdir(folder_path + 'traces-tkt-' + caracteristics + '-' + str(tss_nb))
        for i in range(tss_nb):
            shutil.copyfile(folder_path + 'traces-tkt-' + caracteristics + '-2500/ts_' + str(i) + '.csv',
                            folder_path + 'traces-tkt-' + caracteristics + '-' + str(tss_nb) + '/ts_' + str(i) + '.csv')
    file = open(folder_path+'traces-rtip-'+caracteristics+'-'+str(2500), 'r')
    lines = file.readlines()
    file.close()
    for tss_nb in [50, 100, 500, 1000]:
        file = open(folder_path + 'traces-rtip-' + caracteristics + '-' + str(tss_nb), 'w+')
        file.writelines([str(tss_nb) + ' ' + str(alpha_size) + '\n'] + lines[1:tss_nb+1])
        file.close()
    g.n_tss = 100
    g.empty_tss()
    g.generate(False)
    g.tag_file_writer(folder_path+'traces-negatives-'+caracteristics)
    g.empty_tss()
    g.generate(True)
    g.tag_file_writer(folder_path+'traces-positives-'+caracteristics)
