import pandas as pd
from datetime import datetime, timedelta, date
import numpy as np
import os, sys

sys.path.append("../TAG/")

from TAG.TALearner import TALearner
from TAG.Automaton import Automaton

path = os.path.dirname(sys.argv[0])

widths = [1, 3, 2, 6, 6, 6, 6, 6, 50, 50, 6, 6, 6, 1, 1, 1, 1, 1, 1, 3, 2, 1, 1, 3, 3, 2, 2, 15]

colnames = ["Log_Format_Version", "Class", "Affiliation_Type", "Call_Sign", "Log_Date", "Start_Time", "End_Time", "Duration",
            "Prog_Title_/_avdertiser", "SubTitle_/_Episode_#", "Producer_1", "Producer_2", "Production_#",
            "Special_Attention_Code", "Country_of_Origin", "Broadcast_Origination_Point", "Exhibition",
            "Production_Source", "Target_Audience", "Categories", "Accessible_Programming",
            "Dubbing_/_Dramatic_Credit", "Ethnic_Program_Type", "Language_1", "Language_2", "Group",
            "Locally Relevant", "Filler"]

data = pd.read_fwf(path+"/2020-08\CBET_202008_140345885.log", widths=widths,
                   names=colnames, dtype="str")

data.dropna(axis=0, how='all', inplace=True)
data.reset_index(drop=True, inplace=True)

def strpdelta(s):
    t = datetime.strptime(s, "%H%M%S")
    return timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)

data.Log_Date = data.Log_Date.apply(lambda x: datetime.strptime(x, "%y%m%d"))
data.Start_Time = data.Start_Time.apply(lambda x: datetime.strptime(x, "%H%M%S").time())
data.End_Time = data.End_Time.apply(lambda x: datetime.strptime(x, "%H%M%S").time() if x is not np.nan else np.nan)
data.Duration = data.Duration.apply(lambda x: strpdelta(x) if x is not np.nan else np.nan)

categories = {"010": "News",
              "02A": "News", # Analysis and interpretation
              "02B": "Documentary", # Long-form documentary
              "030": "News", # Reporting and actualities
              "040": "Religion",
              "05A": "Children_programs", # Formal education and pre-school
              "05B": "Children_programs", # Informal education / Recreation and leisure
              "06A": "Sports", # Professional sports
              "06B": "Sports", # Amateur sports
              "07A": "Dramatic_series", # Ongoing dramatic series
              "07B": "Sitcoms",
              "07C": "Films", # Specials, mini-series and made-for-TV feature films
              "07D": "Films", # Theatrical feature films aired on TV
              "07E": "Films", # Animated television programs and films
              "07F": "Humoristic_program", # Programs of comedy sketches, improvisation, unscripted works, stand-up comedy
              "07G": "Dramatic_series", # Other drama
              "08A": "Music_dance", # Music and dance
              "08B": "Music_dance", # Music video clips
              "08C": "Music_dance", # Music video programs
              "090": "Variety",
              "100": "Game_shows",
              "11A": "Magazine_program", # General entertainment & human interest / Canadian entertainment magazine programs
              "11B": "Reality_television",
              "120": "Interstitials",
              "130": "Public_service_announcements",
              "140": "COM"} # Infomercials, promotional and corporate videos

traces = list()
line = ""
day = data.Log_Date.iloc[0]
cat = data.Categories.iloc[0]
current = data.Class.iloc[0]
t = 0
d = 0
deb = 6
fin = 12
for i, row in data.iterrows():
    if row.Log_Date.weekday() != 4: continue # Friday
    if row.Start_Time.hour >= fin and len(line) > 0:
        traces.append(line[:-1] + "\n")
        line = ""
        day = row.Log_Date
        d=0
    if row.Start_Time.hour >= fin or row.Start_Time.hour < deb:
        continue
    if row.Start_Time.hour == deb and row.Start_Time.minute == 0:
        current = row.Class
        cat = row.Categories
    if current != row.Class or (current == row.Class == "PGR" and cat != row.Categories):
        if current == "PGR":
            line += categories[cat] + ":" + str(round(d)) + " "
            d = t
        else:
            if t > 0:
                line += current + ":" + str(round(d)) + " "
                d = t
        t = 0
        current = row.Class
        if current == "PGR": cat = row.Categories
    if len(data) - 1 == i:
        t += row.Duration.total_seconds()
        if current == "PGR":
            if t > 0:
                line += row.Categories + ":" + str(round(d)) + " "
                d = t
        else:
            if t > 0:
                line += row.Class + ":" + str(round(d)) + " "
                d = t
    elif ((datetime.combine(date.today(), row.Start_Time) + row.Duration).time()) != data.iloc[i+1].Start_Time:
        if row.Class == "PGR":
            if data.iloc[i+1].Start_Time < row.Start_Time:
                t = (datetime.combine(date.fromisoformat('1996-10-23'), data.iloc[i+1].Start_Time) - \
                    datetime.combine(date.fromisoformat('1996-10-22'), row.Start_Time) + \
                    timedelta(hours=24)).total_seconds()
            else:
                t = (datetime.combine(date.fromisoformat('1996-10-22'), data.iloc[i+1].Start_Time) - \
                    datetime.combine(date.fromisoformat('1996-10-22'), row.Start_Time)).total_seconds()
            current = "PGR"
        else:
            t += row.Duration.total_seconds()
            if t > 0:
                line += row.Class + ":" + str(round(d)) + " "
                d = t
            if data.iloc[i+1].Start_Time < row.Start_Time:
                t = (datetime.combine(date.fromisoformat('1996-10-23'), data.iloc[i+1].Start_Time) - \
                    datetime.combine(date.fromisoformat('1996-10-22'), row.Start_Time)).total_seconds()
            else:
                t = (datetime.combine(date.fromisoformat('1996-10-22'), data.iloc[i+1].Start_Time) - \
                    datetime.combine(date.fromisoformat('1996-10-22'), row.Start_Time)).total_seconds()
            current = "PGR"
    else:
        t += row.Duration.total_seconds()

file = open(path+"/traces_tag_fri_am", "w+")
file.writelines(traces)
file.close()

l = TALearner(path+"/traces_tag_fri_am")
print(len(l.ta.states))
print(len(l.ta.edges))
print(len(l.ta.symbols))
