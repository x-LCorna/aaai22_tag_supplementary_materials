import pandas as pd
from datetime import datetime, timedelta, date
import numpy as np
import os, sys

sys.path.append("../TAG/")

from TAG.TALearner import TALearner
from TAG.Automaton import Automaton

path = os.path.abspath(os.path.dirname(sys.argv[0]))

widths = [1, 3, 2, 6, 6, 6, 6, 6, 50, 50, 6, 6, 6, 1, 1, 1, 1, 1, 1, 3, 2, 1, 1, 3, 3, 2, 2, 15]

colnames = ["Log_Format_Version", "Class", "Affiliation_Type", "Call_Sign", "Log_Date", "Start_Time", "End_Time", "Duration",
            "Prog_Title_/_avdertiser", "SubTitle_/_Episode_#", "Producer_1", "Producer_2", "Production_#",
            "Special_Attention_Code", "Country_of_Origin", "Broadcast_Origination_Point", "Exhibition",
            "Production_Source", "Target_Audience", "Categories", "Accessible_Programming",
            "Dubbing_/_Dramatic_Credit", "Ethnic_Program_Type", "Language_1", "Language_2", "Group",
            "Locally Relevant", "Filler"]

data = pd.read_fwf(path+"/2020-08\CBET_202008_140345885.log", widths=widths,
                   names=colnames, dtype="str")
data = pd.concat([pd.read_fwf(path+"/2020-07\CBET_202007_125607393.log", widths=widths, names=colnames, dtype="str"), data])

data.dropna(axis=0, how='all', inplace=True)
data.reset_index(drop=True, inplace=True)

def strpdelta(s):
    t = datetime.strptime(s, "%H%M%S")
    return timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)

data.Log_Date = data.Log_Date.apply(lambda x: datetime.strptime(x, "%y%m%d"))
data.Start_Time = data.Start_Time.apply(lambda x: datetime.strptime(x, "%H%M%S").time())
data.End_Time = data.End_Time.apply(lambda x: datetime.strptime(x, "%H%M%S").time() if x is not np.nan else np.nan)
data.Duration = data.Duration.apply(lambda x: strpdelta(x) if x is not np.nan else np.nan)
data.Class.replace({"PRO":"PRC", "NRN":"COM"}, inplace=True)

categories = {"010": "News",
              "02A": "News", # Analysis and interpretation
              "02B": "Documentary", # Long-form documentary
              "030": "News", # Reporting and actualities
              "040": "Religion",
              "05A": "Children_programs", # Formal education and pre-school
              "05B": "Children_programs", # Informal education / Recreation and leisure
              "06A": "Sports", # Professional sports
              "06B": "Sports", # Amateur sports
              "07A": "Dramatic_series", # Ongoing dramatic series
              "07B": "Sitcoms",
              "07C": "Films", # Specials, mini-series and made-for-TV feature films
              "07D": "Films", # Theatrical feature films aired on TV
              "07E": "Films", # Animated television programs and films
              "07F": "Humoristic_program", # Programs of comedy sketches, improvisation, unscripted works, stand-up comedy
              "07G": "Dramatic_series", # Other drama
              "08A": "Music_dance", # Music and dance
              "08B": "Music_dance", # Music video clips
              "08C": "Music_dance", # Music video programs
              "090": "Variety",
              "100": "Game_shows",
              "11A": "Magazine_program", # General entertainment & human interest / Canadian entertainment magazine programs
              "11B": "Reality_television",
              "120": "Interstitials",
              "130": "Public_service_announcements",
              "140": "COM"} # Infomercials, promotional and corporate videos

def delta_hours(t1, t2):
    if t2 < t1:
        d2 = date.fromisoformat('1996-10-23')
    else:
        d2 = date.fromisoformat('1996-10-22')
    return (datetime.combine(d2, t2) - datetime.combine(date.fromisoformat('1996-10-22'), t1)).total_seconds()

traces = list()
line = ""
day = data.Log_Date.iloc[0]
cat, current = None, None
d = 0
cpt = 0
for i, row in data.iterrows():
    if i == len(data) - 1 or day != row.Log_Date:
        if not line: continue
        traces.append(line[:-1]+"\n")
        line = ""
        d = 0
        day = row.Log_Date
        current, cat = None, None
    cpt += 1
    if current is None: # First event of the time slot
        current = row.Class
        c = categories[row.Categories] if current == "PGR" else current
        line += c + ":" + str(round(d)) + " "
        if i == len(data) - 1: d = row.Duration.total_seconds()
        else: d = delta_hours(row.Start_Time, data.iloc[i+1].Start_Time)
        if row.Class == "PGR":
            cat = row.Categories
        continue
    elif current != row.Class or (current == row.Class == 'PGR' and cat != row.Categories):
        c = categories[row.Categories] if row.Class == "PGR" else row.Class
        line += c + ":" + str(round(d)) + " "
        d = 0
        current = row.Class
        if i == len(data) - 1: d = row.Duration.total_seconds()
        else: d = delta_hours(row.Start_Time, data.iloc[i+1].Start_Time)
        if row.Class == "PGR": cat = row.Categories
        continue
    if i != len(data) - 1 and ((datetime.combine(date.today(), row.Start_Time) + row.Duration).time()) < data.iloc[i+1].Start_Time:
        d += row.Duration.total_seconds()
        line += categories[cat] + ":" + str(round(d)) + " "
        d = delta_hours((datetime.combine(date.today(), row.Start_Time) + row.Duration).time(), data.iloc[i+1].Start_Time)
        current = "PGR"
    else:
        if i == len(data) - 1: d = row.Duration.total_seconds()
        else: d += delta_hours(row.Start_Time, data.iloc[i + 1].Start_Time)

print("Number of events:", str(cpt))

file = open(path+"/traces_tag_august", "w+")
file.writelines(traces)
file.close()

import math

def create_location(s_id, pos, name, ub=None):
    if pos is not None:
        loc = "\t\t\t<location id=\"id" + str(s_id) + "\" x=\"" + str(round(pos[0])) + "\" y=\"" + str(round(pos[1])) + "\">" + "\n"
        loc += "\t\t\t\t<name x=\"" + str(round(pos[0])) + "\" y=\"" + str(round(pos[1] + 15)) + "\">" + name + "</name>" + "\n"
    else:
        loc = "\t\t\t<location id=\"id" + str(s_id) + "\">" + "\n"
        loc += "\t\t\t\t<name>" + name + "</name>" + "\n"
    if ub is not None:
       loc +=  '\t\t\t\t<label kind="invariant">d&lt;='+str(ub)+'</label>\n'
    loc += "\t\t\t</location>" + "\n"
    return loc

def create_edge(e, source, target, proba=False):
    edge = "\t\t<transition>" + "\n"
    edge += "\t\t\t<source ref=\"" + source + "\"/>" + "\n"
    edge += "\t\t\t<target ref=\"" + target + "\"/>" + "\n"
    if proba:
        edge += "\t\t\t<label kind=\"probability\">" + str(
            round(e.proba * 100)) + "</label>" + "\n"
    else:
        edge += "\t\t\t<label kind=\"synchronisation\">" + e.symbol + '!' + "</label>" + "\n"
        edge += "\t\t\t<label kind=\"guard\">d&gt;=" + str(min(e.guard)) + \
               " &amp;&amp;d&lt;=" + str(max(e.guard)) + "</label>" + "\n"
        edge += "\t\t\t<label kind=\"assignment\">d:=0</label>" + "\n"
    edge += "\t\t</transition>" + "\n"
    return edge

def ta_to_xml(ta: Automaton):
    s_id = 0
    d_s_id = dict.fromkeys([s.name for s in ta.states] + ta.symbols)
    xml = ""
    branchpoint = ""
    init = ""

    xml += '<?xml version="1.0" encoding="utf-8"?>' + '\n'
    xml += "<!DOCTYPE nta PUBLIC '-//Uppaal Team//DTD Flat System 1.1//EN' 'http://www.it.uu.se/research/group/darts/uppaal/flat-1_2.dtd'>" + '\n'
    xml += '<nta>' + '\n'
    xml += '\t<declaration>' + '\n'

    xml += "broadcast chan "
    for symbol in ta.symbols[:-1]:
        xml += symbol + ', '
    xml += ta.symbols[-1] + ';' + '\n'
    xml += '\t</declaration>' + '\n'

    xml += '\t<template>' + '\n'
    xml += '\t\t<name x="5" y="5">T_Automaton</name>' + '\n'
    xml += '\t\t<declaration>' + '\n'
    xml += 'clock d, t;' + '\n'
    xml += '\t\t</declaration>' + '\n'

    ## PRINCIPAL TA
    # States
    for state in ta.states:
        if state.initial:
            init = '\t\t\t<init ref="'+'id'+str(s_id)+'"/>' + "\n"
            if len(state.edges_out) > 1:
                xml += "\t\t\t<location id=\"id" + str(s_id) + "\">" + "\n"
                xml += "\t\t\t\t<name>INIT</name>\n\t\t\t\t<urgent/>\n\t\t\t</location>\n"

                trans_ini = "\t\t<transition>" + "\n"
                trans_ini += "\t\t\t<source ref=\"id" + str(s_id) + "\"/>" + "\n"
                s_id += 1
                trans_ini += "\t\t\t<target ref=\"id" + str(s_id) + "\"/>" + "\n"
                trans_ini += "\t\t</transition>" + "\n"

        if len(state.edges_out) < 2:
            ub = max(next(iter(state.edges_out)).guard)
            xml += create_location(s_id, None, state.name, ub)
            d_s_id[state.name] = "id" + str(s_id)
            s_id += 1
        else:
            branchpoint += "\t\t\t<branchpoint id=\"id" + str(s_id) + "\">" + "\n"
            d_s_id[state.name] = "id" + str(s_id)
            branchpoint += "\t\t\t</branchpoint>" + "\n"
            s_id += 1
            for e in state.edges_out:
                ub = max(e.guard)
                name = state.name + '_' + e.destination.name + '_' + e.symbol
                xml += create_location(s_id, None, name, ub)
                d_s_id[name] = "id" + str(s_id)
                s_id += 1

    xml += branchpoint + init + trans_ini
    # Transitions
    for state in ta.states:
        if len(state.edges_out) == 1:
            e = state.edges_out.pop()
            xml += create_edge(e, d_s_id[e.source.name], d_s_id[e.destination.name])
        else:
            for e in state.edges_out:
                name = state.name + '_' + e.destination.name + '_' + e.symbol
                # Transition proba
                xml += create_edge(e, d_s_id[state.name], d_s_id[name], proba=True)
                # Transition guard
                xml += create_edge(e, d_s_id[name], d_s_id[e.destination.name])

    xml += "\t</template>"

    ## AUXILLIAIRY AUTOMATON

    k = len(ta.symbols) + 1
    positions = []
    for j in range(0, k):
        positions.append((k*30 * math.cos((2 * math.pi * j) / k),
                         k*30 * math.sin((2 * math.pi * j) / k)))

    xml += "\t<template>" + "\n"
    xml += "\t\t<name>T_Actions</name>" + "\n"
    xml += '\t\t<declaration>' + '\n'
    xml += 'clock '
    for s in ta.symbols: xml += "c_" + s + ', '
    xml = xml[:-2] + ';' + '\n'
    xml += '\t\t</declaration>' + '\n'
    pos = positions.pop()
    xml += create_location(s_id, pos, "INIT")
    d_s_id["INIT"] = "id" + str(s_id)
    s_id += 1
    for s in ta.symbols:
        pos = positions.pop()
        xml += create_location(s_id, pos, 'S_'+s.upper())
        d_s_id[s] = "id" + str(s_id)
        s_id += 1

    xml += "\t\t\t<init ref=\"" + d_s_id["INIT"] + "\"/>" + "\n"
    for s in ta.symbols + ["INIT"]:
        for t in [t for t in ta.symbols if t != s]:
            xml += "\t\t\t<transition>" + "\n"
            xml += "\t\t\t\t<source ref=\"" + d_s_id[s] + "\"/>" + "\n"
            xml += "\t\t\t\t<target ref=\"" + d_s_id[t] + "\"/>" + "\n"
            xml += "\t\t\t\t<label kind=\"synchronisation\">" + t + "?" + "</label>" + "\n"
            if s != 'INIT':
                xml += "\t\t\t<label kind=\"assignment\">c_" + s + ":=0</label>" + "\n"
            xml += "\t\t\t</transition>" + "\n"

    xml += "\t</template>" + "\n"

    xml += '\t<system>' + "\n"
    xml += 'Automaton = T_Automaton();' + "\n"
    xml += 'Actions = T_Actions();' + "\n"
    xml += 'system Automaton, Actions;' + "\n"
    xml += '\t</system>' + "\n"
    xml += '</nta>'
    return xml

l = TALearner(path+"/traces_tag_summer", k=3)
print(len(l.ta.states))
print(len(l.ta.edges))
print(len(l.ta.symbols))
print(l.operations)
file = open(path+'/traces_tag_summer.xml', 'w+')
file.writelines(ta_to_xml(l.ta))
file.close()
